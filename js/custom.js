$('.says').owlCarousel({
    loop:true,
    // autoplay: true,
    // autoplayTimeout: 3000,
    nav:true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});



// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        var scrollpos = target.offset().top;
        var navH = $('#navigation').outerHeight();
        scrollpos = scrollpos-navH-50;
        event.preventDefault();
        $('html, body').animate({
          scrollTop: scrollpos
        }, 100, function() {});
      }
    }
  });



(function(window, $, undefined) {
    'use strict';
    $(document).ready(function() {
        function is_scrolling() {
            var $element = $('#navigation'),
                $nav_height = $element.outerHeight(true);
            if ($(this).scrollTop() >= $nav_height) {
                $element.addClass('is-scrolling');

            } else {
                $element.removeClass('is-scrolling');
            }
        }
        $(window).scroll(_.throttle(is_scrolling, 200));
    });
})(this, jQuery);